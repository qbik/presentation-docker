$c = get-content "$PSScriptRoot\docker-compose.yml"

$volumes = $c | % { 
    if ($_ -match "\""(asgard.legimi.com.*)\""") {
        $matches[1]
    }
 }

$volumes | % {
    Write-Warning "creating volume $_"
    docker volume create --name=$_ -d cifs
}