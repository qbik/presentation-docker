---
theme: black
# theme: cleaver-dark
---

<!-- .slide: data-background="./img/docker-facebook-share.png" -->

---

# Containers technology

--

## The challenge

![challenge](img/challenge.png)

--

## Cargo Transport pre-1960

![challenge](img/cargo-transport-pre-1960.png)

--

## Solution 

Containers!

![container](img/Container-solution.png)

---

# What is a container


* group of processes contained in a **isolated environment**
* isolation provided by underlying OS mechanisms

--

## Containers vs VMs

![containers-vs-vms](img/containers-vms-together@2x.png)

--

## Containers vs VMs

* use underlying OS mechanisms like cgroups and namespaces. (Namespaces deal with resource isolation for a single process, while cgroups manage resources for a group of processes.)

* think of a container as a **lightweight VM**

---

## Docker architecture

![docker-overview](img/docker-overeview.svg)

--

# Use cases

* running dependecies
(mysql, redis, mongo, node, etc.)
* developing
* deployment

---

# DEMO!

--

# Demo architecture

docker client on localhost -> docker service on ubuntu VM (192.168.13.20) on asgard

```
$env:DOCKER_HOST = "tcp://192.168.13.20:2375"
```

--

# Hello-world

Let' start with something simple...

```
docker run hello-world
```

--

> To generate this message, Docker took the following steps:
> 1. The Docker client contacted the Docker daemon.
> 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
> 3. The Docker daemon created a new container from that image which runs the
>    executable that produces the output you are currently reading.
> 4. The Docker daemon streamed that output to the Docker client, which sent it
>    to your terminal.

--

```
docker ps
docker ps -a
docker images
```

--- 

# Containers vs images

* Image - a downloaded image of a system
* Container - a runnable instance, based on some image

--

# Containers vs images

> An instance of an image is called a container. You have an image, which is a set of layers as you describe. 
> If you start this image, you have a running container of this image. You can have many running containers of the same image.
> [SO](https://stackoverflow.com/questions/23735149/docker-image-vs-container)

--

---

# Interacting with containers

--

# Some common arguments

```
--rm - remove container after it's stopped
--name - assign a name to container
-it - run in interactive mode, assing terminal
```

--

## Networking

How to run a real service?

```
docker run redis
```

--

> The server is now ready to accept connections on port 6379

Cool! ...What now?

--

## Networking

Container ports have to be exposed on host.

```
docker run --name redis1 -p 6666:6379 redis
```

```
redis-cli -h 192.168.13.20 -p 6666
> set "key1" "value1"
> get "key1"
```

--

## Links

```powershell
docker run `
        --link redis1:redis_host `
        -it --rm redis redis-cli -h redis_host -p 6379
```

--

# Env variables

```
docker run --rm mysql
```

```
docker run --rm -e MYSQL_ALLOW_EMPTY_PASSWORD=1 mysql
```

--

## Volumes

### Linux

```powershell
docker run \
        -v /myredis/conf/redis.conf:/usr/local/etc/redis/redis.conf \
        --name myredis redis redis-server /usr/local/etc/redis/redis.conf
```


### Windows

```powershell
copy-item samples\04-redis-cfg\redis.conf \\asgard\docker\redis\redis.conf
docker volume create -d cifs --name asgard/docker/redis
docker run `
        -v asgard/docker/redis:/usr/local/etc/redis `
        --name myredis redis redis-server /usr/local/etc/redis/redis.conf `
        -p 6666:6777
```

---

# Docker Compose

* define containers in `docker-compose.yml`

--

<div class="stretch">
        <monaco-editor language="yaml" url="./samples/04-redis-cfg/docker-compose.yml"></monaco-editor>
</div>

--

# Docker Compose

```
docker-compose up redis
docker-compose run redis-cli
docker-compose ps
```

---

# Docker commandline

--

## run vs start

`run` - creates a new *container* based on specified *image*. Can specify additional options, such as port mapping, etc.
`stop/start` - stops/starts *containers* that were created with `run`

--

# exec

```
docker exec -it redis1 bash
```

---

# Building docker images

--

# Dockerfile

--

# Image layers

https://imagelayers.io/?images=redis:3.0.6

---

# Windows and docker

* Windows Containers

---

# Resources

* https://stackoverflow.com/questions/16047306/how-is-docker-different-from-a-normal-virtual-machine
* https://www.docker.com/what-container
* http://why-docker.talk.duyetdev.com
* [shell in a box](https://code.google.com/archive/p/shellinabox/)
