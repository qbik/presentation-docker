# start redis container, expose port 6379
docker run -p 6666:6379 redis

# start CLI on localhost, connect to exposed container port
redis-cli -h 192.168.13.20:6370
# set some values
set "key1" "val1"
get "key1"

# restart container
docker stop "redis-1"
docker start "redis-1"
redis-cli -h 192.168.13.20 -p 6666
# values are persisted
get "key1"

docker stop "redis-1"
# run another container
docker run -p 6379:6379 redis

# the values are not there - this is not the same container
redis-cli -h 192.168.13.20 -p 6666
get "key1"

# show running containers
# see multiple containers with image "redis", each with it's own id and name
docker ps --format "table {{.ID}}\t{{.Names}}\t{{.Image}}\t{{.Status}}\t{{.Ports}}"