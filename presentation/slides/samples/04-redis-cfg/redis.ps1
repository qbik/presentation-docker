# https://hub.docker.com/_/redis/
copy-item $PSScriptRoot\redis.conf \\asgard\docker\redis\redis.conf

docker volume create -d cifs --name asgard/docker/redis
docker run `
        -v asgard/docker/redis:/usr/local/etc/redis `
        --name myredis redis redis-server /usr/local/etc/redis/redis.conf `
        -p 6666:6777