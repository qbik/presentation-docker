#create sql_data volume
docker volume create sql_data
#map sql_data to container's data directory
docker run --name "sql-fts_1" -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Legimi321' -p 1433:1433 -v sql_data:/var/opt/mssql/data/ -d sqlserver-fts


docker ps --format "table {{.ID}}\t{{.Names}}\t{{.Image}}\t{{.Status}}\t{{.Ports}}"