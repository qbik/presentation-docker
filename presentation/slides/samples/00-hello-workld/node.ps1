ipmo require
req posh-docker

docker run node

# we've got our container up and running
docker ps 
<# node.js:

var http = require('http');

http.createServer(function (request, response) {
    response.writeHead(200, {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin' : '*'
    });
    response.end('Hello World\n');
}).listen(1337);

#>

# connect from docker exec
docker exec "node-1" wget http://localhost:1337/ -qO-
