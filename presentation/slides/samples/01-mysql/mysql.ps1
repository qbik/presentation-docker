# start mysql container
docker run  -e MYSQL_ALLOW_EMPTY_PASSWORD=1 mysql
# show running containers
docker ps --format "table {{.ID}}\t{{.Names}}\t{{.Image}}\t{{.Status}}\t{{.Ports}}"