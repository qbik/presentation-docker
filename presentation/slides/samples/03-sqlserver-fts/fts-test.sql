CREATE DATABASE [test1] ON  PRIMARY 
( NAME = N'test1', FILENAME = N'/var/opt/mssql/data/test1.mdf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'test1_log', FILENAME = N'/var/opt/mssql/data/test1_log.ldf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
GO

use test1;


--drop table users;
create table users (
usr_id bigint not null PRIMARY KEY,
usr_name nvarchar(200)
)

insert into users (usr_id, usr_name) 
values (1, 'zażółć gęślą jaźń to jest test')
insert into users (usr_id, usr_name) 
values (2, 'to nie jest test')
insert into users (usr_id, usr_name) 
values (3, 'to jest zupełnie co innego')

SELECT *
FROM users
WHERE FREETEXT (usr_name, 'zupelnie')  
GO  