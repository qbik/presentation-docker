const gulp = require('gulp-npm-run')(require('gulp-help')(require('gulp')));
const debug  = require('gulp-debug');
var livereload = require('gulp-livereload');



gulp.task('livereload', function() {
    gulp.src(['slides/docker.md'])
    .pipe(livereload());
});

gulp.task('copy-to-share', function() {
  gulp.src(["slides/docker.md"])
  .pipe(gulp.dest('\\\\asgard\\docker\\revealjs'))
  .pipe(debug());
  gulp.src(["slides/img/**/*"])
  .pipe(gulp.dest('\\\\asgard\\docker\\revealjs\\img'))
  .pipe(debug());
  gulp.src(["slides/samples/**/*"])
  .pipe(gulp.dest('\\\\asgard\\docker\\revealjs\\samples'))
  .pipe(debug());
  gulp.src(["slides/theme/**/*"])
  .pipe(gulp.dest('\\\\asgard\\docker\\revealjs\\theme'))
  .pipe(debug());
});


 
 
gulp.task('watch', function() {
  //livereload.listen({ port: 35728 });
   gulp.start('copy-to-share');
  gulp.watch(['slides/docker.md','slides/theme/*.css'], ['copy-to-share']);
});
