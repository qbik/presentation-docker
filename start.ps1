pushd 

try {

cd "$psscriptroot/presentation"

# ./prepare-volumes.ps1
docker-compose up -d --build

$dockerhost = "localhost" 
if ($env:docker_host -ne $null) {
    $dockerhost = (new-object "System.Uri" $env:docker_host).Host
}
$url = "http://$($dockerhost):8000/docker.md"
write-host "opening $url"
start $url
#gulp watch

} finally {
    popd
}